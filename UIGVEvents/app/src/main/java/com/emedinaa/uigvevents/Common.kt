package com.emedinaa.uigvevents

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.recyclerview.widget.RecyclerView
import java.io.IOException
import java.io.InputStream


fun RecyclerView.Adapter<*>.getBitmapFromAssets(context:Context,
    photo:String):Bitmap{
    val assetManager = context.assets

    var istr: InputStream? = null
    try {
        istr = assetManager.open(photo)
    } catch (e: IOException) {
        e.printStackTrace()
    }

    return BitmapFactory.decodeStream(istr)
}

fun ViewModel.readJsonFromAssets(context:Context,jsonFile:String):String?{
    val assetManager = context.assets
    var json: String? = null
    try {
        val  inputStream:InputStream = assetManager.open(jsonFile)
        json = inputStream.bufferedReader().use{it.readText()}
    } catch (ex: Exception) {
        ex.printStackTrace()
        return null
    }
    return json
}