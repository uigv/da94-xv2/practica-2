package com.emedinaa.uigvevents

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.emedinaa.uigvevents.adapter.NewsAdapter
import com.emedinaa.uigvevents.model.News
import kotlinx.android.synthetic.main.news_fragment.*


class NewsFragment : Fragment() {

    companion object {
        fun newInstance() = NewsFragment()
    }

    private lateinit var viewModel: ViewModel
    private var adapter: NewsAdapter?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.news_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ViewModel::class.java)
        viewModel.news.observe(this,renderNews)
        recyclerViewNews.layoutManager= LinearLayoutManager(activity)

        activity?.let {
            adapter= NewsAdapter(it, emptyList())
            recyclerViewNews.adapter= adapter
            viewModel.retrieveNews(it)
        }
    }

    private val renderNews= Observer<List<News>> {
        adapter?.update(it)
    }

}
