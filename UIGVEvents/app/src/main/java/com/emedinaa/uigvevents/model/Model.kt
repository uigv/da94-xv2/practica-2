package com.emedinaa.uigvevents.model

data class News(val id:Int,val title:String,val desc:String,val photo:String)

data class Event(val id:Int,val title:String,val desc:String,val photo:String)

data class NewsData(val data:List<News>)
data class EventData(val data:List<Event>)