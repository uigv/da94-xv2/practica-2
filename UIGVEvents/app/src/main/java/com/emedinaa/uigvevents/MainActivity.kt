package com.emedinaa.uigvevents

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener {
            var fragment:Fragment?=null

            when(it.itemId){
                R.id.menuNews -> fragment= NewsFragment.newInstance()
                R.id.menuEvents -> fragment= EventsFragment.newInstance()
                R.id.menuBlog -> fragment= BlogFragment.newInstance()
            }

            fragment?.let {
                changeFragment(it)
            }
            return@setOnNavigationItemSelectedListener true
        }

        //first tab
        changeFragment(NewsFragment.newInstance())
    }

    private fun changeFragment(fragment:Fragment){
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.frameLayout,fragment,null)
            commit()
        }
    }
}
