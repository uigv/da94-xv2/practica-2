package com.emedinaa.uigvevents.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.emedinaa.uigvevents.R
import com.emedinaa.uigvevents.getBitmapFromAssets
import com.emedinaa.uigvevents.model.Event
import com.emedinaa.uigvevents.model.News

class NewsAdapter(val context: Context, var data:List<News>):RecyclerView.Adapter<NewsAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view= LayoutInflater.from(parent.context)
            .inflate(R.layout.row,parent,false)
        return  NewsAdapter.ViewHolder (view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder:ViewHolder, position: Int) {
        val item= data[position]

        val title = item.title
        val desc = item.desc
        val photo = "images/".plus(item.photo)

        holder.textViewTitle.text = title
        holder.textViewDesc.text = desc
        holder.imageView.setImageBitmap(getBitmapFromAssets(context,photo))
    }

    fun update( nList:List<News>){
        this.data= nList
    }

    class ViewHolder(v: View): RecyclerView.ViewHolder(v){
        val textViewTitle: TextView =v.findViewById(R.id.textViewTitle)
        val textViewDesc: TextView = v.findViewById(R.id.textViewDesc)
        val imageView: ImageView = v.findViewById(R.id.imageView)
    }
}