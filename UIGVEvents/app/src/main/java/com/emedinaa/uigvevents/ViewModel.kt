package com.emedinaa.uigvevents

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData
import com.emedinaa.uigvevents.model.Event
import com.emedinaa.uigvevents.model.EventData
import com.emedinaa.uigvevents.model.News
import com.emedinaa.uigvevents.model.NewsData
import com.google.gson.Gson

class ViewModel : ViewModel() {

    private val _news = MutableLiveData<List<News>>().apply { value = emptyList() }
    val news: LiveData<List<News>> = _news


    private val _events = MutableLiveData<List<Event>>().apply { value = emptyList() }
    val events: LiveData<List<Event>> = _events


    fun retrieveNews(context:Context){
        val json = readJsonFromAssets(context,"news.json")?:""
        val data= Gson().fromJson(json,NewsData::class.java)

        data?.let {
            _news.value= it.data
        }

    }

    fun retrieveEvents(context:Context){
        val json = readJsonFromAssets(context,"events.json")
        val data= Gson().fromJson(json,EventData::class.java)

        data?.let {
            _events.value= it.data
        }
    }
}
