package com.emedinaa.uigvevents


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.emedinaa.uigvevents.adapter.EventsAdapter
import com.emedinaa.uigvevents.model.Event
import kotlinx.android.synthetic.main.fragment_events.*

/**
 * A simple [Fragment] subclass.
 */
class EventsFragment : Fragment() {

    companion object {
        fun newInstance() = EventsFragment()
    }

    private lateinit var viewModel: ViewModel
    private var adapter:EventsAdapter?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_events, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ViewModel::class.java)
        viewModel.events.observe(this,renderEvents)

        recyclerViewEvents.layoutManager= LinearLayoutManager(activity)

        activity?.let {
            adapter= EventsAdapter(it, emptyList())
            recyclerViewEvents.adapter= adapter
            viewModel.retrieveEvents(it)
        }

    }

    private val renderEvents= Observer<List<Event>> {
        adapter?.update(it)
    }

}
